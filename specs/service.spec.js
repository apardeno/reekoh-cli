'use strict'

let _ = require('lodash')
let path = require('path')
let CliError = require('../lib/cli-error')
let chai = require('chai')
let chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

global.Promise = require('bluebird')

let fs = Promise.promisifyAll(require('fs'))

let Service = require('../service')
let service = new Service()
let expect = chai.expect
chai.should()

let pathToManifest = path.resolve(process.cwd(), './test-plugin/reekoh.yml')
let manifestDir = path.dirname(pathToManifest)
let userData = {
  username: 'user@reekoh.com',
  password: 'userPassword',
  account: '2926e402-820c-52ea-9ac6-bd65931f5284',
  idToken: 'sample_token_here',
  tokenType: 'bearer'
}

describe('Get user credentials', () => {
  let data = _.pick(userData, ['username', 'password'])

  it('Should return an object with username and password properties', () => {
    return service.getUserCredentials(data).should.eventually.be.eql(data)
  })
})

describe('Save active user data', () => {
  it('Should return passed/saved user data', () => {
    return service.saveUserData(userData).should.eventually.be.eql(userData)
  })
})

describe('Get active user data', () => {
  it('Should return saved user data', () => {
    return service.getUserData().should.eventually.be.eql(userData)
  })
})

describe('Remove active user data', () => {
  let error

  before(done => {
    service.removeUserData().then(done).catch(err => {
      error = err
      done()
    })
  })

  it('Should have no error(s)', () => {
    return expect(error).to.be.an('undefined')
  })

  it('Should return an error if no user is logged in.', () => {
    return service.removeUserData().should.be.rejectedWith(CliError)
  })
})

describe('Get plugin details from manifest', () => {
  let pluginManifest
  let error

  before(done => {
    service.getPluginDetails(pathToManifest).then(data => {
      pluginManifest = data
      done()
    }).catch(err => {
      error = err
      done()
    })
  })

  it('Should have no error(s)', () => {
    return expect(error).to.be.an('undefined')
  })

  it('Should return plugin details in json format', () => {
    return pluginManifest.should.be.an('object')
  })

  it('Should return plugin details with base properties', (done) => {
    pluginManifest.should.have.property('apiVersion')
    pluginManifest.should.have.property('kind')
    pluginManifest.should.have.deep.property('metadata.name')
    pluginManifest.should.have.deep.property('metadata.release')
    done()
  })
})

describe('Validate file base on restrictions', () => {
  let iconResult
  let iconError
  let notesContent
  let manifest = {
    version: '1.1.0',
    kind: 'plugin',
    metadata: {
      release: {
        notes: './RELEASENOTES.md'
      }
    }
  }

  let notes = {
    manifestDir,
    key: 'metadata.release.notes',
    validExtensions: ['md'],
    getContent: true
  }

  before(done => {
    let notesDir = path.resolve(manifestDir, manifest.metadata.release.notes)
    let getNotesContent = fs.readFileAsync(notesDir, 'utf8').then(data => {
      notesContent = data
      return Promise.resolve()
    })

    let validateFile = service.validateFile(notes, manifest).then(data => {
      iconResult = data
      return Promise.resolve()
    }).catch(err => {
      iconError = err
      return Promise.resolve()
    })

    Promise.all([getNotesContent, validateFile]).then(() => {
      done()
    })
  })

  it('Should validate file base on restrictions and return an object with its file content set as a property', (done) => {
    expect(iconError).to.be.an('undefined')

    expect(iconResult).to.be.an('object')
    let version = 'version'
    expect(iconResult).to.have.property(version, manifest[version])
    let kind = 'kind'
    expect(iconResult).to.have.property(kind, manifest[kind])
    let metadata = 'metadata'
    expect(iconResult).to.have.property(metadata, manifest[metadata])
    expect(iconResult).to.have.deep.property(notes.key, notesContent)

    done()
  })
})
